export const rotate = (image, angle = 0) => {
  const canvas = document.createElement("canvas");
  canvas.width = image.width;
  canvas.height = image.height;
  var cache = image;
  const ctx = canvas.getContext("2d");
  const centerX = cache.width / 2;
  const centerY = cache.height / 2;
  ctx.save();
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.translate(centerX, centerY);
  ctx.rotate((Math.PI / 180) * angle);
  ctx.drawImage(
    image,
    -cache.width / 2,
    -cache.height / 2,
    cache.width,
    cache.height
  );
  ctx.restore();
  return canvas;
};

export const crop = (image, bounds) => {
  if (!bounds) return image;
  const canvas = document.createElement("canvas");
  canvas.width = image.width;
  canvas.height = image.height;
  const ctx = canvas.getContext("2d");
  const { upper, left, cropWidth, cropHeight } = _scaleBounds(bounds, {
    width: image.width,
    height: image.height,
  });
  debugger;
  ctx.drawImage(
    image,
    left,
    upper,
    cropWidth,
    cropHeight,
    0,
    0,
    canvas.width,
    canvas.height
  );
  return canvas;
};

const _scaleBounds = (bounds, destination) => {
  debugger;
  let upper, left, lower, right, cropHeight, cropWidth;
  upper = destination.height * (bounds.bounds[0].y / bounds.size.height);
  left = destination.width * (bounds.bounds[0].x / bounds.size.width);
  lower = destination.height * (bounds.bounds[1].y / bounds.size.height);
  right = destination.width * (bounds.bounds[1].x / bounds.size.width);
  cropWidth = right - left;
  cropHeight = lower - upper;
  return { upper, left, lower, right, cropWidth, cropHeight };
};

export const mergeSliceWithBase = ({ slice, baseImage, onComplete }) => {
  debugger;
  const baseCanvas = rotate(baseImage, 0); //rot 0 just to convert to canvas
  const context = baseCanvas.getContext("2d");
  const sliceImage = new Image();
  sliceImage.src = URL.createObjectURL(slice.blob);
  sliceImage.onload = (e) => {
    URL.revokeObjectURL(e.target.src);
    const { left, upper, cropHeight, cropWidth } = _scaleBounds(slice.bounds, {
      width: baseCanvas.width,
      height: baseCanvas.height,
    });
    context.drawImage(
      rotate(sliceImage, -slice.rotation),
      left,
      upper,
      cropWidth,
      cropHeight
    );
    baseCanvas.toBlob((blob) => {
      onComplete(blob);
    });
  };
};

export const recalculateBounds = (bounds, mode) => {
  if (!bounds) return;
  switch (mode) {
    case "square":
      let width = bounds.bounds[1].x - bounds.bounds[0].x;
      let height = bounds.bounds[1].y - bounds.bounds[0].y;
      if (width !== height) {
        width = height > width ? height : width;
        height = width > height ? width : height;
      }
      bounds.bounds = [
        { x: bounds.bounds[0].x, y: bounds.bounds[0].y },
        { x: bounds.bounds[0].x + width, y: bounds.bounds[0].y + height },
      ];
      break;
    default:
    //do nothing
  }
  return bounds;
};
