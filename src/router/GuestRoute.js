import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import { Ellipsis } from "react-spinners-css";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const isLoading = useSelector((redux) => redux.isLoading);
  const isAuthenticated = useSelector((redux) => redux.user.isAuthenticated);
  return (
    <Route
      {...rest}
      render={(props) =>
        isLoading ? (
          <div>
            <Ellipsis color="#7a7676" size={32} />
          </div>
        ) : !isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );
};
export default PrivateRoute;
