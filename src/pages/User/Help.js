import React from "react";
import { Link } from "react-router-dom";

const Help = () => {
  return (
    <div>
      <h1>Trouble signing In?</h1>
      <h2>I can't sign in to my account</h2>
      If you're having trouble accessing your account you can{" "}
      <Link to="/user/reset">reset your password</Link>
      <h2>I can't access my content</h2>
      If you are a member of a group, your group administrator needs to add you
      to the license. Please contact them first.
    </div>
  );
};
export default Help;
