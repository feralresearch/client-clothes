import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { umgrGetUsers } from "state/actions";
import { SortableTable } from "@theplacelab/ui";

const defaultAv = "https://assets.theplacelab.com/demo/user.png";

export const User = ({ user }) => {
  return (
    <React.Fragment>
      <div style={{ textAlign: "left" }}>{user.name}</div>
      <div style={{ textAlign: "left" }}>{user.email}</div>
      <div>{user.role}</div>
      <div>
        <img
          alt={user.email}
          onError={(e) => {
            e.target.onError = null;
            e.target.src = defaultAv;
          }}
          style={{ width: "2rem" }}
          src={user.avatar ? user.avatar : defaultAv}
        />
      </div>
      <div>
        {user.f_validated} | {user.f_reset}
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          textAlign: "left",
        }}
      >
        <div>[delete]</div>
        <div>[delete]</div>
      </div>
    </React.Fragment>
  );
};

const Manage = () => {
  const dispatch = useDispatch();
  const userList = useSelector((redux) => redux.userManager.users);
  useEffect(() => {
    dispatch(umgrGetUsers());
  }, [dispatch]);

  if (!userList || userList.length === 0) return <div>Loading...</div>;

  const headers = [
    { label: "Account", align: "left", width: "20rem", sortable: true },
    { label: "B" },
    { label: "C" },
    { label: "D" },
    { label: "E" },
    { label: "F" },
  ];
  const tableData = [];
  userList.forEach((user) => {
    tableData.push([
      <div>
        {user.name} {user.email}
      </div>,
      <div>2</div>,
      <div>3</div>,
      <div>4</div>,
      <div>5</div>,
      <div>6</div>,
    ]);
  });

  return (
    <div style={{ maxWidth: "60rem", margin: "auto", marginTop: "10rem" }}>
      <h1>Manage Users</h1>
      <SortableTable
        data={tableData}
        headers={headers}
        options={{ searchOn: 0, filterOn: 0 }}
      />
    </div>
  );
};
export default Manage;
