import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div style={{ margin: "10rem" }}>
      <div>Hello, you are logged in!</div>
      <Link to="/">Home</Link>
    </div>
  );
};
export default Home;
