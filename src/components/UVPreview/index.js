import React from "react";

const UVPreview = ({ style, rotation, uv, size, isActive, onClick }) => {
  const { width, height } = size;
  const styles = {
    ...style,
    theme_editor: {
      ...style.theme_editor,
      width,
      height,
      background: "radial-gradient(#e66465, #9198e5)",
    },

    theme_panel: {
      ...style.theme_panel,

      boxShadow: isActive
        ? `inset 0px 0px 0px 1px ${style.theme_highlight}`
        : "",
    },
  };
  return (
    <div
      style={styles.theme_panel}
      onMouseDown={onClick}
      onTouchStart={onClick}
    >
      <div>
        <div
          style={{
            flexGrow: "1",
            overflow: "clip",
            width,
            height,
            borderRadius: "0.2rem",
          }}
        >
          <img
            id="uvPreview"
            style={{
              transform: `rotate(${rotation}deg)`,
              width,
              height,
            }}
            alt=""
            src={uv}
          />
        </div>
        {isActive && <div style={styles.theme_controls}>export</div>}
      </div>
    </div>
  );
};

export default UVPreview;
