import React, { useState } from "react";

import { SignIn } from "@theplacelab/auth";
import { userLogin, userLogout } from "../../state/actions";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import PrivateLink from "../../router/PrivateLink";

const styles = {
  container: {
    position: "fixed",
    display: "flex",
    alignItems: "center",
    background: "grey",
    color: "white",
    padding: "1rem",
    width: "100vw",
    top: 0,
    zIndex: 1,
  },
};

function Navigation() {
  const dispatch = useDispatch();
  return (
    <div style={styles.container}>
      <div style={{ display: "flex", flexGrow: 1 }}>
        <Link to="/">Home</Link>
        <div>
          <PrivateLink to="/private">Private</PrivateLink>
          <PrivateLink requires={["websuper"]} to="/users">
            Manage Users
          </PrivateLink>
        </div>
      </div>
      <div
        style={{ display: "flex", alignItems: "center", marginRight: "2rem" }}
      >
        <SignIn
          authService={window._env_.REACT_APP_AUTH_SERVICE}
          authHelper={window._env_.REACT_APP_AUTH_HELPER}
          onLogin={(payload) => dispatch(userLogin(payload))}
          onLogout={(payload) => dispatch(userLogout(payload))}
          onStatus={(status) => {
            const event = new Event("statusUpdate");
            event.data = {
              backgroundColor: "orange",
              icon: "cat",
              ...status,
            };
            window.dispatchEvent(event);
          }}
        />
      </div>
    </div>
  );
}

export default Navigation;
