import React, { useEffect, useState } from "react";
import FBXViewer from "components/FBXViewer";
import UVEditor from "components/UVEditor";
import { recalculateBounds, mergeSliceWithBase } from "util/image";
import UVPreview from "components/UVPreview";
import ModelSelector from "components/ModelSelector";
import availableModels from "modelsManifest.json";
import Iconify from "./Iconify";

const Designer = ({ style }) => {
  const styles = {
    ...style,
    page: {
      display: "flex",
      color: "white",
    },
    col: {
      display: "flex",
      flexDirection: "column",
      flexGrow: "1",
    },
    row: { display: "flex", flexDirection: "row", flexGrow: "1" },
  };

  const onSelectModel = (model) => {
    setModel(model);
    setFbxPath(`models/${model.folder}/object.fbx`);
  };

  const onSelectMaterial = (folderName) => {
    setActiveEditor(null);
    setCurrentUv(`models/shirt/materials/${folderName}/base_color.png`);
    setMaterialPath(`models/shirt/materials/${folderName}`);
  };

  const getMaterialFromPath = (path) =>
    path.substring(path.lastIndexOf("/") + 1);

  const [model, setModel] = useState(null);
  const [fullUv, setFullUv] = useState(true);
  const [activeEditor, setActiveEditor] = useState(null);
  const [fbxPath, setFbxPath] = useState(null);
  const [materialPath, setMaterialPath] = useState(null);
  const [currentUv, setCurrentUv] = useState(`${materialPath}/base_color.png`);

  const rotation = 270;

  useEffect(() => {
    if (!fbxPath && availableModels.length > 0) {
      setModel(availableModels[0]);
      setFbxPath(`models/${availableModels[0].folder}/object.fbx`);
      onSelectMaterial(availableModels[0].materials[0].folder);
    }
  }, [fbxPath]);

  const onApply = (data) => {
    if (fullUv) {
      setCurrentUv(URL.createObjectURL(data.blob));
    } else {
      const baseImage = new Image();
      baseImage.src = data.uv;
      baseImage.onload = () => {
        mergeSliceWithBase({
          slice: {
            blob: data.blob,
            rotation: data.rotation,
            bounds: data.targetArea,
          },
          baseImage,
          onComplete: (blob) => setCurrentUv(URL.createObjectURL(blob)),
        });
      };
    }
  };

  return (
    <div style={styles.page}>
      <ModelSelector
        style={style}
        models={availableModels}
        onSelect={onSelectModel}
      />
      <div
        style={{ ...styles.theme_pagePanel, padding: ".5rem .5rem 5rem .5rem" }}
      >
        <div style={styles.row}>
          <div>
            <UVEditor
              onClick={() => setActiveEditor("uv")}
              isActive={activeEditor === "uv"}
              rotation={fullUv ? 0 : rotation}
              uv={currentUv}
              editorSize={{ width: 512, height: 512 }}
              textureSize={{ width: 2048, height: 2048 }}
              targetArea={
                fullUv ? null : recalculateBounds(model?.targetArea, "square")
              }
              onApply={onApply}
              style={styles}
              controlsMain={
                <div
                  style={{
                    ...styles.row,
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  {model?.targetArea && (
                    <div onClick={() => setFullUv(!fullUv)}>
                      {fullUv ? (
                        <Iconify icon="controls-editor-crop-marks" />
                      ) : (
                        <Iconify icon="controls-editor-crop" />
                      )}
                    </div>
                  )}
                  <div style={{ flexGrow: 1 }}></div>
                  {model?.materials?.map((material) => (
                    <div
                      key={material.folder}
                      onClick={(e) => onSelectMaterial(material.folder)}
                    >
                      <div
                        style={{
                          width: 20,
                          height: 20,
                          borderRadius: 20,
                          border: `2px solid ${
                            getMaterialFromPath(materialPath) ===
                            material.folder
                              ? style.theme_highlight
                              : "black"
                          }`,
                          backgroundColor: `${material.color}`,
                          marginLeft: ".5rem",
                        }}
                        title={material.name}
                      />
                    </div>
                  ))}
                </div>
              }
            />
          </div>
          <div>
            <FBXViewer
              onClick={() => setActiveEditor("fbx")}
              isActive={activeEditor === "fbx"}
              size={{ width: 300, height: 300 }}
              fbx={fbxPath}
              material={materialPath}
              uv={currentUv}
              showStats={false}
              style={styles}
            />
            <UVPreview
              onClick={() => setActiveEditor("preview")}
              isActive={activeEditor === "preview"}
              size={{ width: 300, height: 300 }}
              style={styles}
              uv={currentUv}
              rotation={rotation}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Designer;
