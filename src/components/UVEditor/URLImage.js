import React, { useState, useEffect } from "react";
import { Image } from "react-konva";
import * as imgHelper from "../../util/image";

const URLImage = ({ src, rotation, targetArea, size }) => {
  const [image, setImage] = useState(null);
  useEffect(() => {
    const handleLoad = () => {
      setImage(
        imgHelper.rotate(imgHelper.crop(image, targetArea, size), rotation)
      );
    };
    let image = new window.Image();
    image.src = src;
    image.crossOrigin = "Anonymous";
    image.addEventListener("load", handleLoad);
    return () => {
      image.removeEventListener("load", handleLoad);
    };
  }, [targetArea, rotation, size, src]);

  return (
    <Image
      width={size.width}
      height={size.height}
      image={image}
      onClick={(e) => {
        const stage = e.target.getStage();
        console.log({
          size,
          x: e.evt.clientX - stage.attrs.container.offsetLeft,
          y: e.evt.clientY - stage.attrs.container.offsetTop + window.scrollY,
        });
      }}
    />
  );
};
export default URLImage;
